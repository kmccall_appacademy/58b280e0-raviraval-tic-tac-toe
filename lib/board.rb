# require 'byebug'
class Board
  # debugger
  attr_reader :grid

  def initialize(grid = [[nil, nil, nil], [nil, nil, nil], [nil, nil, nil]])
    @grid = grid
  end

  def place_mark(pos, mark)
    grid[pos[0]][pos[1]] = mark
  end

  def empty?(pos)
    return true if grid[pos[0]][pos[1]] == nil
  end

  def winner
    #rows
    return :X if grid[0].all? { |el| el == :X }
    return :X if grid[1].all? { |el| el == :X }
    return :X if grid[2].all? { |el| el == :X }
    return :O if grid[0].all? { |el| el == :O }
    return :O if grid[1].all? { |el| el == :O }
    return :O if grid[2].all? { |el| el == :O }
    #columns
    return :X if grid.each_index.all? { |idx| grid[idx][0] == :X }
    return :X if grid.each_index.all? { |idx| grid[idx][1] == :X }
    return :X if grid.each_index.all? { |idx| grid[idx][2] == :X }
    return :O if grid.each_index.all? { |idx| grid[idx][0] == :O }
    return :O if grid.each_index.all? { |idx| grid[idx][1] == :O }
    return :O if grid.each_index.all? { |idx| grid[idx][2] == :O }
    #diagonals
    return :X if grid.each_index.all? { |idx| grid[idx][idx] == :X}
    return :O if grid.each_index.all? { |idx| grid[idx][idx] == :O}
    return :X if grid[0][2] == :X && grid[1][1] == :X && grid[2][0] == :X
    return :O if grid[0][2] == :X && grid[1][1] == :X && grid[2][0] == :O
  end

  def over?
    return true unless winner == nil
    return true if grid.each.none? { |row| row.include?(nil) }
  end
end
