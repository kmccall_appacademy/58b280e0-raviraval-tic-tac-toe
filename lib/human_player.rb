class HumanPlayer
  attr_accessor :mark
  attr_reader :name

  def initialize(name)
    @mark = :X
    @name = name
  end

  def get_move
    puts "where do you want to place your mark? enter as row, column"
    player_move = gets.chomp
    player_move_arr = [player_move[0].to_i, player_move[3].to_i]
  end

  def display(board)
    print board.grid
  end
end
