# require 'byebug'
require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'
class Game
  attr_accessor :current_player, :board, :player_one, :player_two

  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @board = Board.new
    @current_player = player_one
  end

  def play_turn
    # debugger
    puts "---"
    current_player.display(board)
    puts
    puts "#{current_player.name}, your turn!"
    board.place_mark(current_player.get_move, current_player.mark)
    switch_players!
  end

  def switch_players!
    if @current_player == player_one
      @current_player = player_two
    else
      @current_player = player_one
    end
  end

  def play
    until board.over?
      play_turn
    end
    print board.grid
    puts
    puts "Congratulations!"
  end

end

if $PROGRAM_NAME == __FILE__
  while true
    puts "Select your choice:"
    puts "(1) Player vs Computer"
    puts "(2) Player vs Player"
    user_choice = gets.chomp
    if user_choice == "1"
      puts "Player, type your name:"
      player_name = gets.chomp
      Game.new(HumanPlayer.new(player_name) ,ComputerPlayer.new("Evil Computer")).play
    elsif user_choice == "2"
      puts "Player one, type your name:"
      name1 = gets.chomp
      puts "Player two, type your name:"
      name2 = gets.chomp
      Game.new(HumanPlayer.new(name1) ,HumanPlayer.new(name2)).play
    end
  end

end
