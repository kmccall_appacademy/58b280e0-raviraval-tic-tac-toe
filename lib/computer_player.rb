class ComputerPlayer
  attr_accessor :mark, :board
  attr_reader :name
  def initialize(name)
    @name = name
    @mark = :O
  end

  def display(board)
    @board = board
  end

  def get_move
    dupped_board = board.dup
    #searches for a winning move
    empty_slots = []
    dupped_board.grid.each_index do |rowidx|
      dupped_board.grid[rowidx].each_index do |posidx|
        if dupped_board.grid[rowidx][posidx] == nil
          dupped_board.grid[rowidx][posidx] = mark
          if dupped_board.winner == mark
            return [rowidx, posidx]
          else
            dupped_board.grid[rowidx][posidx] = nil
            empty_slots << [rowidx, posidx]
          end
        end
      end
    end
    #if we didn't find a winning move, randomly pick one of those empty_slots
    empty_slots.sample
  end
end
